*Distribution of scaled earnings*
use "C:\Users\toshiba\Documents\replication\BDdata.dta", clear
xtset gvkey yeara
drop if dnum>4400 & dnum<5000
drop if dnum>6000 & dnum<6500
keep if data25!=. & data199!=.
gen MV=data25*data199
gen lagMV=l.MV
gen sca_earn= data172/lagMV
tabstat sca_earn,by (yeara) stat(n p1 p99 me sd med p25 p75)
drop if sca_earn>0.8571982 & yeara==1976
drop if sca_earn<-2.011514 & yeara==1976
drop if sca_earn>0.8088408 & yeara==1977
drop if sca_earn<-1.570233 & yeara==1977
drop if sca_earn>0.7892039 & yeara==1978
drop if sca_earn<-1.164319 & yeara==1978
drop if sca_earn>0.9373 & yeara==1979
drop if sca_earn<-1.1321 & yeara==1979
drop if sca_earn>0.81 & yeara==1980
drop if sca_earn<-1.715 & yeara==1980
drop if sca_earn>0.7633 & yeara==1981
drop if sca_earn<-1.9684 & yeara==1981
drop if sca_earn>1.0339 & yeara==1982
drop if sca_earn<-1.9683 & yeara==1982
drop if sca_earn>0.9833 & yeara==1983
drop if sca_earn<-2.1012 & yeara==1983
drop if sca_earn>0.5652 & yeara==1984
drop if sca_earn<-1.3509 & yeara==1984
drop if sca_earn>0.6022 & yeara==1985
drop if sca_earn<-2.1418 & yeara==1985
drop if sca_earn>0.5759 & yeara==1986
drop if sca_earn<-2.433 & yeara==1986
drop if sca_earn>0.6275 & yeara==1987
drop if sca_earn<-1.848 & yeara==1987
drop if sca_earn>0.6776 & yeara==1988
drop if sca_earn<-2.109 & yeara==1988
drop if sca_earn>0.5826 & yeara==1989
drop if sca_earn<-1.9756 & yeara==1989
drop if sca_earn>0.5058 & yeara==1990
drop if sca_earn<-2.6839 & yeara==1990
drop if sca_earn>0.7622 & yeara==1991
drop if sca_earn<-5.525 & yeara==1991
drop if sca_earn>0.8570 & yeara==1992
drop if sca_earn<-2.9596 & yeara==1992
drop if sca_earn>0.8795 & yeara==1993
drop if sca_earn<-1.9903 & yeara==1993
drop if sca_earn>0.4314 & yeara==1994
drop if sca_earn<-1.3422 & yeara==1994
tabstat sca_earn,by (yeara) stat(n me sd med p25 p75)
histogram sca_earn if sca_earn<0.35 & sca_earn>-0.25, width(0.005) start(-0.25) frequency xtitle(Earnings Interval) xline(0, lpattern(dash)) xlabel(-0.25(0.05)0.35)

*Distribution graph of subsamples: sca_earn*
gen group1= l.sca_earn<0
gen group2= l.sca_earn>0 & l2.sca_earn<0
gen group3= l.sca_earn>0 & l2.sca_earn>0 & l3.sca_earn<0
gen group4= group2+group3
gen group5=1-group1-group4
histogram sca_earn if sca_earn>-0.25 & sca_earn<0.35, width(0.005) start(-0.25) frequency xline(0, lpattern(dash)) xlabel(-0.25(0.05)0.35) by(group1)
histogram sca_earn if sca_earn>-0.25 & sca_earn<0.35, width(0.005) start(-0.25) frequency xline(0, lpattern(dash)) xlabel(-0.25(0.05)0.35) by(group4)
histogram sca_earn if sca_earn>-0.25 & sca_earn<0.35, width(0.005) start(-0.25) frequency xline(0, lpattern(dash)) xlabel(-0.25(0.05)0.35) by(group5)

*Distribution of scaled earnings changes*
use "C:\Users\toshiba\Documents\replication\BDdata.dta", clear
xtset gvkey yeara
drop if dnum>4400 & dnum<5000
drop if dnum>6000 & dnum<6500
keep if data25!=. & data199!=.
gen MV=data25*data199
gen lagE=l.data172
gen chae=data172-lagE
gen lag2MV=l2.MV
gen sca_chae=chae/lag2MV
tabstat sca_chae,by (yeara) stat(n p1 p99 me sd med p25 p75)
drop if sca_chae>1.5405 & yeara==1977
drop if sca_chae<-1.123 & yeara==1977
drop if sca_chae>1.424 & yeara==1978
drop if sca_chae<-0.797& yeara==1978
drop if sca_chae>1.1507 & yeara==1979
drop if sca_chae<-0.9918 & yeara==1979
drop if sca_chae>1.0212 & yeara==1980
drop if sca_chae<-1.579 & yeara==1980
drop if sca_chae>1.5475 & yeara==1981
drop if sca_chae<-1.3896 & yeara==1981
drop if sca_chae>1.2563 & yeara==1982
drop if sca_chae<-1.3628 & yeara==1982
drop if sca_chae>1.7746 & yeara==1983
drop if sca_chae<-1.389 & yeara==1983
drop if sca_chae>2.1285 & yeara==1984
drop if sca_chae<-1.5013 & yeara==1984
drop if sca_chae>0.9028 & yeara==1985
drop if sca_chae<-1.1417 & yeara==1985
drop if sca_chae>1.7299 & yeara==1986
drop if sca_chae<-1.3512 & yeara==1986
drop if sca_chae>1.7207 & yeara==1987
drop if sca_chae<-1.1333 & yeara==1987
drop if sca_chae>1.2142 & yeara==1988
drop if sca_chae<-1.126 & yeara==1988
drop if sca_chae>1.4929 & yeara==1989
drop if sca_chae<-1.4085 & yeara==1989
drop if sca_chae>1.4278 & yeara==1990
drop if sca_chae<-1.5588 & yeara==1990
drop if sca_chae>1.6020 & yeara==1991
drop if sca_chae<-1.4549 & yeara==1991
drop if sca_chae>3.5560 & yeara==1992
drop if sca_chae<-2.0367 & yeara==1992
drop if sca_chae>2.4370 & yeara==1993
drop if sca_chae<-2.3256 & yeara==1993
drop if sca_chae>1.6530& yeara==1994
drop if sca_chae<-1.8365 & yeara==1994
tabstat sca_chae,by (yeara) stat(n me sd med p25 p75)
histogram sca_chae if sca_chae>-0.15 & sca_chae<0.15, width(0.0025) start(-0.15) frequency xtitle(Change in Earnings Interval) xline(0, lpattern(dash)) xlabel(-0.15(0.05)0.15) graphregion(margin(zero))

*Distribution graph of subsamples: sca_chae*
gen group1= l.sca_chae<0
gen group2= l.sca_chae>0 & l2.sca_chae<0
gen group3= l.sca_chae>0 & l2.sca_chae>0 & l3.sca_chae<0
gen group4= group2+group3
gen group5=1-group1-group4
histogram sca_chae if sca_chae>-0.15 & sca_chae<0.15, width(0.0025) start(-0.15) frequency xline(0, lpattern(dash)) xlabel(-0.15(0.05)0.15) by(group1)
histogram sca_chae if sca_chae>-0.15 & sca_chae<0.15, width(0.0025) start(-0.15) frequency xline(0, lpattern(dash)) xlabel(-0.15(0.05)0.15) by(group4)
histogram sca_chae if sca_chae>-0.15 & sca_chae<0.15, width(0.0025) start(-0.15) frequency xline(0, lpattern(dash)) xlabel(-0.15(0.05)0.15) by(group5)

*Generating quantile graph*
import excel "C:\Users\toshiba\Documents\replication\BDtrialData.xlsx", sheet("Sheet1") firstrow
xtset gvkey yeara
sort sca_earn
bys group: egen m=median(sca_earn)
gen CA=data2+data3+data68
gen sca_ca=CA/MV
bys group: egen sca_ca1=pctile( sca_ca), p(75)
bys group: egen sca_ca2=pctile( sca_ca), p(50)
bys group: egen sca_ca3=pctile( sca_ca), p(25)
twoway (connected sca_ca1 sca_ca2 sca_ca3 m, sort) if m>-0.1 & m<0.2, ytitle(Quantiles of CA) xtitle(Median Portfolio Earnings) xline(0, lpattern(dash)) xlabel(-0.1(0.05)0.2)

gen CL= data70+ data71+ data72
gen sca_cl=CL/MV
bys group: egen sca_cl1=pctile( sca_cl), p(75)
bys group: egen sca_cl2=pctile( sca_cl), p(50)
bys group: egen sca_cl3=pctile( sca_cl), p(25)
twoway (connected sca_cl1 sca_cl2 sca_cl3 m, sort) if m>-0.1 & m<0.2, ytitle(Quantiles of CL)  xtitle(Median Portfolio Earnings) xline(0, lpattern(dash)) xlabel(-0.1(0.05)0.2)

replace data110=0 if missing(data110)
replace data308=0 if missing(data308)
gen CFO=data110+data308
gen sca_cfo=CFO/MV
bys group: egen sca_cfo1=pctile( sca_cfo), p(75)
bys group: egen sca_cfo2=pctile( sca_cfo), p(50)
bys group: egen sca_cfo3=pctile( sca_cfo), p(25)
twoway (connected sca_cfo1 sca_cfo2 sca_cfo3 m, sort) if m>-0.1 & m<0.2, ytitle(Quantiles of CFO) yline(0,lpattern(dash)) xtitle(Median Portfolio Earnings) xline(0, lpattern(dash)) xlabel(-0.1(0.05)0.2)

xtset gvkey yeara
gen CWC=(data2-l.data2+data3-l.data3+data68-l.data68)-(data70-l.data70+data71-l.data71+data72-l.data72)
gen sca_cwc=CWC/MV
bys group: egen sca_cwc1=pctile( sca_cwc), p(75)
bys group: egen sca_cwc2=pctile( sca_cwc), p(50)
bys group: egen sca_cwc3=pctile( sca_cwc), p(25)
twoway (connected sca_cwc1 sca_cwc2 sca_cwc3 m, sort) if m>-0.1 & m<0.2, ytitle(Quantiles of CWC) yline(0,lpattern(dash)) xtitle(Median Portfolio Earnings) xline(0, lpattern(dash)) xlabel(-0.1(0.05)0.2)

gen OA=data172-CFO-CWC
gen sca_oa=OA/MV
bys group: egen sca_oa1=pctile( sca_oa), p(75)
bys group: egen sca_oa2=pctile( sca_oa), p(50)
bys group: egen sca_oa3=pctile( sca_oa), p(25)
twoway (connected sca_oa1 sca_oa2 sca_oa3 m, sort) if m>-0.1 & m<0.2, ytitle(Quantiles of OA) yline(0,lpattern(dash)) xtitle(Median Portfolio Earnings) xline(0, lpattern(dash)) xlabel(-0.1(0.05)0.2)
